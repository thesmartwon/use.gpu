declare module "@use-gpu/wgsl/render/sample/cube-to-omni.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getCubeToOmniSample: ParsedBundle;
  export default __module;
}
