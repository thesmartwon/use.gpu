declare module "@use-gpu/wgsl/pmrem/pmrem-diffuse-sh.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const pmremDiffuseSH: ParsedBundle;
  export default __module;
}
