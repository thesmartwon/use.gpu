declare module "@use-gpu/wgsl/transform/stereographic.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getStereographicPosition: ParsedBundle;
  export default __module;
}
