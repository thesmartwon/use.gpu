declare module "@use-gpu/wgsl/transform/web-mercator.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getWebMercatorPosition: ParsedBundle;
  export default __module;
}
