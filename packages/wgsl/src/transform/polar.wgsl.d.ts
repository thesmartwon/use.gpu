declare module "@use-gpu/wgsl/transform/polar.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getPolarPosition: ParsedBundle;
  export default __module;
}
