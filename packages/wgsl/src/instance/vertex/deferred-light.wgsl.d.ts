declare module "@use-gpu/wgsl/instance/vertex/deferred-light.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getDeferredLightVertex: ParsedBundle;
  export default __module;
}
