declare module "@use-gpu/wgsl/instance/fragment/deferred-light.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getDeferredLightFragment: ParsedBundle;
  export default __module;
}
