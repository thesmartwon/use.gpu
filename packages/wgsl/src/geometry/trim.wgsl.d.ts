declare module "@use-gpu/wgsl/geometry/trim.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getLineTrim: ParsedBundle;
  export default __module;
}
